Run this application
Step 1
type following command
>> npm install
>> npm run dev

Change database configuration in datastore.js file
/config/datastore.js
create a Database in mysql name project_db
**API Description**
1) **url**: localhost:1337/accessories
   Method: GET
   result: [
      {
            "createdAt": 1572271698986,
            "updatedAt": 1572271698986,
            "id": 4,
            "line_item": "Accessories Sales",
            "functional_level": "Accounting",
            "group_level": "Level1",
            "period": "1",
            "cfy_value": "2",
            "cfy_count": "3",
            "budget_value": "4",
            "budget_count": "5",
            "1718_value": "6",
            "1718_count": "7",
            "1617_value": "8",
            "1617_count": "9"
        }
    ]

2)**url**: localhost:1337/accessories?line_item=Accessories Sales&functional_level=Accounting&group_level=Level1&period=1&cfy_value=2&cfy_count=3&budget_value=4&budget_count=5&value_1718=6&count_1718=7&value_1617=8&count_1617=9
  Method :POST
  result : ok

3)**url**: localhost:1337/accessories/uploadjsonfile
  Method: POST
  result: ok
