/**
 * Accessories.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https:                                                      //sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    line_item: {
      type: "string"
    },
    functional_level: {
      type: "string"
    },
    group_level: {
      type: "string"
    },
    period: {
      type: "string"
    },
    cfy_value: {
      type: "string"
    },
    cfy_count: {
      type: "string"
    },
    budget_value: {
      type: "string"
    },
    budget_count: {
      type: "string"
    },
    "1718_value": {
      type: "string"
    },
    "1718_count": {
      type: "string"
    },
    "1617_value": {
      type: "string"
    },
    "1617_count": {
      type: "string"
    }
  }
};
