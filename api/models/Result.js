/**
 * Result.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https: //sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    userid: {
      model: "User",
      columnName: "userId"
    },
    gameid: {
      model: "Game",
      columnName: "gameId"
    },
    result: { type: "string" },
    completestatus: { type: "string" },
    count: { type: "string" }

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝

    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
  }
};
