/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https: //sailsjs.com/docs/concepts/actions
 */
const Joi = require("@hapi/joi");
module.exports = {
  create: async function(req, res) {
    let { username, password } = req.allParams();
    let result = await User.create({
      username,
      password,
      logincount: 0
    }).fetch();
    return res.ok("ok");
  },
  login: async function(req, res) {
    try {
      const schema = Joi.object({
        username: Joi.string().required(),
        password: Joi.string().required()
      });
      let { username, password } = await schema.validateAsync(req.allParams());
      let result = await User.findOne({ username, password });
      if (result) {
        let userid = result.id;
        let gameresult = await sails.models.result.find({
          userid: userid,
          completestatus: false
        });
        let userGameFullResult = await sails.models.result
          .find({ userid: userid})
          .populate("userid")
          .populate("gameid");
        let gameResultLength = gameresult.length;
        if (gameResultLength > 0) {
          if (gameResultLength < 5) {
            let remainCount = 5 - gameResultLength;
            console.log( 'remainCount = ', remainCount);
            let gamesList = await sails.models.game.find();
            let count = 0;
            let updateNewGame = [];
            gamesList.forEach(element => {
              var found = userGameFullResult.find(function(gameElement) {
                if (gameElement.gameid.id == element.id) {
                  return true;
                }
              });
              //found undefined means not add game to the user
              if(typeof found == 'undefined' && remainCount >0){
                let newGame = {};
                newGame.userid = userid;
                newGame.gameid = element.id;
                newGame.result = 0;
                newGame.completestatus = false;
                newGame.count = 0;
                updateNewGame.push(newGame);
                remainCount--;
              }
            });
            if (updateNewGame.length > 0) {
              let createResult = await sails.models.result
                .createEach(updateNewGame)
                .fetch();
            }
          }
          let gamebelow70 = [];
          let game_70_100 = [];
          gameresult.forEach(element => {
            if (element.result < 70) {
              gamebelow70.push(element);
            } else {
              game_70_100.push(element);
            }
          });
          let rerult = gamebelow70.concat(game_70_100);
          return res.ok(rerult);
        } else {
          let games = await sails.models.game.find();
          let length = games.length > 5 ? 5 : games.length;
          let queryArr = [];
          for (let i = 0; i < length; i++) {
            let element = {};
            element.userid = userid;
            element.gameid = games[i].id;
            element.result = 0;
            element.completestatus = false;
            element.count = 0;
            queryArr.push(element);
          }
          let createResult = await sails.models.result
            .createEach(queryArr)
            .fetch();
          return res.ok(createResult);
        }
      } else {
        return res.badRequest("Invalid Credentials");
      }
    } catch (err) {
      if (err.name == "ValidationError") {
        return res.badRequest(err);
      }
      return res.serverError(err);
    }
  }
};
