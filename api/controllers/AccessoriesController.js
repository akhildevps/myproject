/**
 * AccessoriesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const Joi = require("@hapi/joi");
module.exports = {
  /**
   * Method :GET
   * url: localhost:1337/accessories
   */
  find: async (req, res) => {
    try {
      let result = await Accessories.find();
      return res.ok(result);
    } catch (err) {
      return res.serverError(err);
    }
  },
  /**
   * Method :POST
   * url: localhost:1337/accessories?line_item=Accessories Sales&functional_level=Accounting&group_level=Level1&period=1&cfy_value=2&cfy_count=3&budget_value=4&budget_count=5&value_1718=6&count_1718=7&value_1617=8&count_1617=9
   *
   */
  create: async (req, res) => {
    try {
      const schema = Joi.object({
        line_item: Joi.string().required(),
        functional_level: Joi.string(),
        group_level: Joi.string(),
        period: Joi.string(),
        cfy_value: Joi.string(),
        cfy_count: Joi.string(),
        budget_value: Joi.string(),
        budget_count: Joi.string(),
        value_1718: Joi.string(),
        count_1718: Joi.string(),
        value_1617: Joi.string(),
        count_1617: Joi.string()
      });
      const {
        line_item,
        functional_level,
        group_level,
        period,
        cfy_value,
        cfy_count,
        budget_value,
        budget_count,
        value_1718,
        count_1718,
        value_1617,
        count_1617
      } = await schema.validateAsync(req.allParams());
      const result = await Accessories.create({
        line_item,
        functional_level,
        group_level,
        period,
        cfy_value,
        cfy_count,
        budget_value,
        budget_count,
        "1718_value": value_1718,
        "1718_count": count_1718,
        "1617_value": value_1617,
        "1617_count": count_1617
      });
      return res.ok(result);
    } catch (err) {
      if (err.name == "ValidationError") {
        return res.badRequest(err);
      }
      return res.serverError(err);
    }
  },
  uploadjsonfile: async (req, res) => {
    try {
      var fs = require("fs");
      var content = fs.readFileSync("assets/uploads/content.json");
      var jsonContent = JSON.parse(content);
      var createdAccessories = await Accessories.createEach(
        jsonContent
      ).fetch();
      return res.ok('ok');
    } catch (err) {
      return res.serverError(err);
    }
  },
  /**
   * Method :GET
   * url: localhost:1337/ping
   */
  ping: (req, res) => {
    console.log('-------------------------------------[1]');
    return res.ok("ok");
  },
  mail: async (req, res) => {
    console.log('---------------mail---------------------');
    try {
      const schema = Joi.object({
        email: Joi.string()
          .required()
          .email(),
        password: Joi.string(),
        receiver:Joi.string()
        .required()
        .email(),
        subject: Joi.string().required(),
        message: Joi.string().required()
      });
      let { email, password, receiver, subject, message } = await schema.validateAsync(req.allParams());
      var nodemailer = require("nodemailer");
      var transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
          user: email,
          pass: password
        }
      });

      var mailOptions = {
        from: email,
        to: receiver,
        subject: subject,
        html:message
      };

      transporter.sendMail(mailOptions, function(error, info) {
        if (error) {
          return res.serverError(error);
        } else {
          return res.ok("Email sent: " + info.response);
        }
      });
    } catch (err) {
      if (err.name == "ValidationError") {
        return res.badRequest(err);
      }
      return res.serverError(err);
    }
  }
};
